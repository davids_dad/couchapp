FROM python:2.7

RUN pip install couchapp \
&& mkdir couchapp

WORKDIR couchapp

ENTRYPOINT ["couchapp"]
CMD ["--help"]